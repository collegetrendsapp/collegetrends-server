#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  registration.py
#  
#  Copyright 2017 salaciouscrumb <salaciouscrumb@salaciouscrumb-Inspiron-3537>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
from __init__ import app
from flask import Blueprint, request, flash, url_for, redirect, render_template, jsonify, session
from sqlalchemy import exc
import re
from models import Users, Students, Faculty, Admins, UserCommunity, Streams, StreamYears
from models import db
from flask_jwt_extended import (
    JWTManager, jwt_required, create_access_token,
    jwt_refresh_token_required, create_refresh_token,
    get_jwt_identity, set_access_cookies,
    set_refresh_cookies, unset_jwt_cookies
)
register = Blueprint('register', __name__)

def validate_fields(mobile, email):
	if not re.search(r"^[789]\d{9}$", mobile):
		return False
	elif email and not re.search(r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)", email):
		return False
	else:
		return True

def complete(form, reqFields):
	for field in reqFields:
		if form[field] is None:
			return False
	return True

def register_faculty_fn(form):
	if 'email' not in form:
		e_mail = ''
	else:
		e_mail = form['email']
	if validate_fields(form['mobile'], e_mail) == True:
		db.create_all()
		user = Users(mobile = form['mobile'], fname = form['fname'],
			lname = form['lname'], email = e_mail, pwd = form['pwd'], role = Users.ROLE_FACULTY)
		stream_to_int = int(form['stream'])
		try:
			db.session.add(user)
			db.session.commit()
			curUser = Users.query.filter_by(mobile = form['mobile']).first()
			faculty = Faculty(curUser, stream_to_int)
			community = db.session.query(Streams).filter(Streams.id == stream_to_int).first().community
			user_comm = UserCommunity(curUser.id, community)
			db.session.add(user_comm)
			db.session.add(faculty)
			db.session.commit()
		except exc.IntegrityError:
			db.session.rollback()
			return jsonify({ 'response': 4, 'msg': "Integrity constraint error" })
		return jsonify({ 'response': 0, 'msg': "Success" })
	else:
		return jsonify({ 'response': 2, 'msg': "Invalid fields" })

def register_student_fn(form):
	stream_list = {"CSE": 1, "ECE": 2, "IT": 3, "ME": 4, "EE": 5}
	if 'email' not in form:
		e_mail = ''
	else:
		e_mail = form['email']
	if validate_fields(form['mobile'], e_mail) == True:
		db.create_all()
		user = Users(mobile = form['mobile'], fname = form['fname'],
			lname = form['lname'], email = e_mail, pwd = form['pwd'], role = Users.ROLE_STUDENT)
		stream_to_int = int(form['stream'])
		if(int(form['year'])>4):
			raise IntegrityError('Only four years allowed')
		try:
			db.session.add(user)
			db.session.commit()
			curUser = Users.query.filter_by(mobile = form['mobile']).first()
			student = Students(curUser, stream_to_int, int(form['year']))
			community = db.session.query(Streams).filter(Streams.id == stream_to_int).first().community
			user_comm = UserCommunity(curUser.id, community)
			db.session.add(user_comm)
			year_comm = db.session.query(StreamYears).filter(StreamYears.stream == stream_to_int).filter(StreamYears.no ==  int(form['year'])).first().community
			user_comm_year = UserCommunity(curUser.id, year_comm)
			db.session.add(user_comm_year)
			db.session.add(student)
			db.session.commit()
			
		except exc.IntegrityError:
			db.session.rollback()
			return jsonify({ 'response': 4, 'msg': "Integrity constraint error" })
		return jsonify({ 'response': 0, 'msg': "Success" })
	else:
		return jsonify({ 'response': 2, 'msg': "Invalid fields" })

def register_admin_fn(form):
	if 'email' not in form:
		e_mail = ''
	else:
		e_mail = form['email']
	if validate_fields(form['mobile'], e_mail) == True:
		db.create_all()
		user = Users(mobile = form['mobile'], fname = form['fname'],
			lname = form['lname'], email = e_mail, pwd = form['pwd'], role = Users.ROLE_ADMIN)
		try:
			db.session.add(user)
			db.session.commit()
			curUser = Users.query.filter_by(mobile = form['mobile']).first()
			admin = Admins(curUser)
			db.session.add(admin)
			db.session.commit()
		except exc.IntegrityError:
			db.session.rollback()
			return jsonify({ 'response': 4, 'msg': "Integrity constraint error" })
		return jsonify({ 'response': 0, 'msg': "Success" })
	else:
		return jsonify({ 'response': 2, 'msg': "Invalid fields" })

@register.route('/api/register/students', methods = ['POST'])
def register_student():
	if request.method == 'POST':
		if not complete(request.form, ['mobile', 'pwd', 'stream', 'year', 'fname', 'lname']):
			return jsonify({ 'response': 1, 'msg': "Incomplete form" })
		else:
			try:
				return register_student_fn(request.form)
			except Exception:
				return jsonify({ 'response': 3, 'msg': "Bad request" })			
	else:
		return jsonify({ 'response': 3, 'msg': "Bad request" })

@register.route('/api/register/faculty', methods = ['POST'])
def register_faculty():
	stream_list = {"CSE": 0, "EE": 1, "ECE": 2, "ME": 3}
	if request.method == 'POST':
		if not complete(request.form, ['mobile', 'pwd', 'stream', 'fname', 'lname']):
			return jsonify({ 'response': 1, 'msg': "Incomplete form" })
		else:
			try:
				return register_faculty_fn(request.form)
			except Exception:
				return jsonify({ 'response': 3, 'msg': "Bad request" })		
	else:
		return jsonify({ 'response': 3, 'msg': "Bad request" })

@register.route('/api/register/admin', methods = ['POST'])
def register_admin():
	if request.method == 'POST':
		if not complete(request.form, ['mobile', 'pwd', 'fname', 'lname']):
			return jsonify({ 'response': 1, 'msg': "Incomplete form" })
		else:
			try:
				return register_admin_fn(request.form)
			except Exception:
				return jsonify({ 'response': 3, 'msg': "Bad request" })		
	else:
		return jsonify({ 'response': 3, 'msg': "Bad request" })		

@register.route('/api/register/import', methods = ['POST'])
def import_student():
	if request.method == 'POST':
		if not complete(request.form, ['csv']):
			return jsonify({ 'response': 1, 'msg': "Incomplete form" })
		else:
			try:
				csv =  request.form['csv']
				csv_lines = csv.split('\n')
				for line in csv_lines:
					entry = line.split(',')
					if len(entry) == 7:
						form = {'fname': entry[0].strip(), 'lname': entry[1].strip(), 'mobile': entry[2].strip(), 'pwd': entry[3],
									'email': entry[4].strip(), 'stream': int(entry[5].strip()), 'year': int(entry[6].strip()) }
						register_student_fn(form)
				return jsonify({ 'response': 0, 'msg': "Success" })
			except KeyError:
				return jsonify({ 'response': 3, 'msg': "Bad request" })		
	else:
		return jsonify({ 'response': 3, 'msg': "Bad request" })		