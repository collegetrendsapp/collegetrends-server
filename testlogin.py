from login import loginsuccess, loginfail
import unittest

class KnownLogins(unittest.TestCase) :
	known_logins=((9147483647, '12345'))
	def test_loginsuccess(self) :
		for mobile,pwd in self.known_logins :
			result=login.loginsuccess(mobile,pwd)
			self.assertTrue(result)
	def test_loginfail(self) :
		for mobile,pwd in self.known_logins :
			result=login.loginfail(mobile,pwd)
			self.assertEqual(True,result)

if __name__=='main' :
	unittest.main()

