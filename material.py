#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  material.py
#  
#  Copyright 2017 salaciouscrumb <salaciouscrumb@salaciouscrumb-Inspiron-3537>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
from __init__ import app
from flask import Blueprint, request, flash, url_for, redirect, render_template, jsonify, session
from sqlalchemy import exc, or_, and_, desc
from user_identity import UserIdentity
import json, random, jsonpickle
import re, datetime, uuid
from models import Users, Faculty, Materials, DiscThreads, UserCommunity, Tags, MaterialTags, Communities
from models import db
from flask_jwt_extended import (
    JWTManager, jwt_required, create_access_token,
    jwt_refresh_token_required, create_refresh_token,
    get_jwt_identity, set_access_cookies, get_jwt_claims,
    set_refresh_cookies, unset_jwt_cookies
)
import html

materials = Blueprint('materials', __name__)

def count_tags(form):
    count = 0
    if 'tag_0' not in form or form['tag_0'] is None:
        return count
    else:
        count = count + 1
        if 'tag_1' not in form or form['tag_1'] is None:
            return count
        else:
            count = count + 1
            if 'tag_2' not in form or form['tag_2'] is None:
                return count
            else:
                count = count + 1
                if 'tag_3' not in form or form['tag_3'] is None:
                    return count
                else: return count + 1

def get_tags(form, count):
    tags = []
    for i in range(0, count):
        tags.append(form['tag_' + str(i)])
    return tags

def complete(form, reqFields):
    for field in reqFields:
        if field not in form or not form[field]:
            return False
    return True
#test this
def upload_material_fn(form, user, faculty_id):
    thread = uuid.uuid4().urn[9:29]
    title = form['title'].replace('\n'," ")
    community = int(form['community'])
    content = form['content'].replace("\n"," ")
    link = form['link']
    if len(title.strip()) <= 0 or len(content.strip()) < 0:
        return jsonify({'success': -1}), 201 #bad request
    private = db.session.query(Communities).filter(Communities.id == community).first().private
    valid = True
    if private:
        valid = db.session.query(UserCommunity).filter(UserCommunity.cid == community).\
        filter(UserCommunity.uid == user).first()
    if valid is None:
        return jsonify({'success':  -2}) #faculty cannot post in this community
    material = Materials(title,content,link, faculty_id, community, thread)
    thread_entry = DiscThreads(thread, community)
    try:
        db.session.add(thread_entry)
        db.session.commit()
        db.session.add(material)
        db.session.commit()
        cur_material = db.session.query(Materials).filter(Materials.thread == thread).first()
        #section for adding tags
        no_tags = count_tags(form)
        tag_list = get_tags(form, no_tags)
        for tag in tag_list:
            exists = db.session.query(Tags).filter(Tags.name == tag.lower()).first()
            if not exists:
                new_tag = Tags(tag)
                db.session.add(new_tag)
                db.session.commit()
                new_tag_id = db.session.query(Tags).filter(Tags.name == tag.lower()).first().id
                mat_tag = MaterialTags(cur_material.id, new_tag_id)
                db.session.add(mat_tag)
                db.session.commit()
            else:
                mat_tag = MaterialTags(cur_material.id, exists.id)
                db.session.add(mat_tag)
                db.session.commit()
        return jsonify({'success': 1, 'thread_id': thread, 'material_id': cur_material.id})
    except KeyError:
        db.session.rollback()
        return jsonify({'success': -3 })

@materials.route('/api/materials/upload', methods = ['POST'])
@jwt_required
def upload_material():
    user = get_jwt_identity()
    faculty_id = UserIdentity.get_faculty_id(get_jwt_claims(), user)
    if faculty_id == -1:
        return jsonify({ 'success': -1 }), 401 #invalid access
    try:
        if complete(request.form, ['title', 'content', 'link', 'community']) == False:
            return jsonify({'success': 0 }), 201
        return upload_material_fn(request.form, user, faculty_id)
        
    except Exception:
        return jsonify({ 'success':  -4 }), 201
    return jsonify({ 'success':  -5 })

@materials.route('/api/materials/get', methods = ['POST'])
@jwt_required
def get_materials_community():
    user = get_jwt_identity()
    type = 0
    role = get_jwt_claims()['roles']
    if role != Users.ROLE_STUDENT:
        type = 1
    try:
        if complete(request.form, ['community']) == False:
            return jsonify([{'success': 0 }]), 201
        community_id = int(request.form['community'])
        if community_id < 0:
            return jsonify([{'success':  -1}]) #no results
        check_valid = [True]
        if type == 0:
            check_valid = db.session.query(Communities, UserCommunity, Users).filter(Communities.id == UserCommunity.cid).filter(Communities.id == community_id).\
                filter(UserCommunity.uid == Users.id).first()
        else:
            private = db.session.query(Communities).filter(Communities.id == community_id).first().private
            if private == 1:
                check_valid = db.session.query(Communities, UserCommunity, Users).filter(Communities.id == UserCommunity.cid).filter(Communities.id == community_id).\
                 filter(UserCommunity.uid == Users.id).first()
        if check_valid is None or len(check_valid) == 0:
            return jsonify([{'success':  -1}]) #no results
        results = db.session.query(Communities, Materials, Faculty, Users).filter(Materials.community == Communities.id).filter(Communities.id == community_id).\
             filter(Materials.faculty == Faculty.id).filter(Faculty.user == Users.id).all()
        resp = []
        for result in results:
            taglist = db.session.query(Tags, MaterialTags).filter(MaterialTags.material == result[1].id).filter(MaterialTags.tag == Tags.id).all()
            tag_arr = []
            for tag in taglist:
                tag_arr.append(tag[0].name)
            resp.append({'id': result[1].id, 'title': html.unescape(result[1].title), 'content': html.unescape(result[1].content), 'link': result[1].link, 'faculty_fname': result[3].fname, 'faculty_lname': result[3].lname, 'thread': result[1].thread, 'timestamp': result[1].timestamp,'tags': str(tag_arr) })
        return jsonify([{'success':  1}, resp]), 200
    except KeyError:
        return jsonify([{ 'success':  -2 }]), 201 #DB error
    return jsonify([{ 'success':  -3 }]) #fallback