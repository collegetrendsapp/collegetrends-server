#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  discussion.py
#  
#  Copyright 2017 salaciouscrumb <salaciouscrumb@salaciouscrumb-Inspiron-3537>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
from __init__ import app
from flask import Blueprint, request, flash, url_for, redirect, render_template, jsonify, session
from sqlalchemy import exc, or_, and_, desc
from user_identity import UserIdentity
import json, random, jsonpickle
import re, datetime
from models import Users, Faculty, DiscThreads, ThreadPosts, Communities, UserCommunity
from models import db
from flask_jwt_extended import (
    JWTManager, jwt_required, create_access_token,
    jwt_refresh_token_required, create_refresh_token,
    get_jwt_identity, set_access_cookies, get_jwt_claims,
    set_refresh_cookies, unset_jwt_cookies
)
import traceback
import html


discuss = Blueprint('discuss', __name__)

def complete(form, reqFields):
	for field in reqFields:
		if field not in form or not form[field]:
			return False
	return True

@discuss.route('/api/discuss/comment', methods = ['POST'])
@jwt_required
def discuss_post():
	try:
		user = get_jwt_identity()
		type = 0
		role = get_jwt_claims()['roles']
		if role != Users.ROLE_STUDENT:
			type = 1
	except Exception:
		return jsonify({ 'success': -1 }), 401 #invalid access
	try:
		if complete(request.form, ['thread', 'content']) == False:
			return jsonify({'success': 0 }), 201
		thread = request.form['thread']
		content = request.form['content'].replace('\n'," ")
		valid = db.session.query(DiscThreads).filter(DiscThreads.id == thread).first().id
		if valid != thread:
			return jsonify({ 'success': 0 }), 201 #invalid details, same as above
		check_valid = [True]
		if type == 0:
			check_valid = db.session.query(DiscThreads, UserCommunity).filter(DiscThreads.id == thread).filter(UserCommunity.cid == DiscThreads.community).\
													filter(UserCommunity.uid == user).first()
		else:
			private = db.session.query(Communities, DiscThreads).filter(Communities.id == DiscThreads.community).first()[0].private
			if private == 1:
				check_valid = db.session.query(DiscThreads, UserCommunity).filter(DiscThreads.id == thread).filter(UserCommunity.cid == DiscThreads.community).\
													filter(UserCommunity.uid == user).first()
		if check_valid is None:
			return jsonify({'success':  -3}) #user cannot post in this community
		discuss = ThreadPosts(thread, user, role, content)
		try:
			db.session.add(discuss)
			db.session.commit()
			return jsonify({'success': 1, 'thread_id': thread, 'content': content })
		except Exception:
			db.session.rollback()
			return jsonify({'success': -4 })
	except Exception as e:
		return jsonify({ 'success':  -5 }), 201
	return jsonify({ 'success':  -6 })

@discuss.route('/api/discuss/view', methods = ['POST'])
@jwt_required
def view_thread():
	user = get_jwt_identity()
	type = 0
	role = get_jwt_claims()['roles']
	if role != Users.ROLE_STUDENT:
		type = 1
	try:
		if complete(request.form, ['thread']) == False:
			return jsonify([{'success': 0 }]), 201
		thread = request.form['thread']
		valid = db.session.query(DiscThreads).filter(DiscThreads.id == thread).first().id
		if valid != thread:
			return jsonify([{ 'success': 0 }]), 201 #invalid details, same as above
		check_valid = [True]
		if type == 0:
			check_valid = db.session.query(DiscThreads, UserCommunity).filter(DiscThreads.id == thread).filter(UserCommunity.cid == DiscThreads.community).\
													filter(UserCommunity.uid == user).first()
		else:
			private = db.session.query(Communities, DiscThreads).filter(Communities.id == DiscThreads.community).first()[0].private
			if private == 1:
				check_valid = db.session.query(DiscThreads, UserCommunity).filter(DiscThreads.id == thread).filter(UserCommunity.cid == DiscThreads.community).\
													filter(UserCommunity.uid == user).first()
		if check_valid is None or len(check_valid) == 0:
			return jsonify([{'success':  -3 }]) #user cannot post in this community
		thread_content = db.session.query(ThreadPosts).filter(ThreadPosts.thread == thread).all()
		count = 0
		if thread_content is None or len(thread_content) == 0:
			return jsonify([{'success': 1, 'count': count}]), 200
		else:
			resp = []
			for post in thread_content:
				count = count + 1
				#we could have an if else for specific types of users here, but seems unnecessary atm
				user_details = db.session.query(Users).filter(Users.id == post.user).first()
				resp.append({'content': html.unescape(post.content), 'user_id': post.user, 'user_fname': user_details.fname, 'user_lname': user_details.lname,
									'user_level': post.userlevel, 'timestamp': post.timestamp })
			return jsonify([{'success':  1, 'count': count}, resp]), 200
	except Exception as e:
		return jsonify([{ 'success':  -1 }]), 201
	return jsonify([{ 'success':  -2 }])