#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  notifications.py
#  
#  Copyright 2017 salaciouscrumb <salaciouscrumb@salaciouscrumb-Inspiron-3537>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
from __init__ import app
from flask import Blueprint, request, flash, url_for, redirect, render_template, jsonify, session
from sqlalchemy import exc, or_, and_, desc
from user_identity import UserIdentity
import json, random, jsonpickle
import re, datetime, pytz
from models import Users, Faculty, Notifications, Communities, UserCommunity, UserNotifMaps, Materials, Schedules, Students
from models import db
from flask_jwt_extended import (
    JWTManager, jwt_required, create_access_token,
    jwt_refresh_token_required, create_refresh_token,
    get_jwt_identity, set_access_cookies, get_jwt_claims,
    set_refresh_cookies, unset_jwt_cookies
)

notifs = Blueprint('notifs', __name__)

def complete(form, reqFields):
	for field in reqFields:
		if field not in form or not form[field]:
			return False
	return True

def add_notif(user, dt, schedule, faculty_id, community, topic):
	try:
		try: 
			time = datetime.datetime.strptime(dt, "%d/%m/%Y %H:%M:%S %z").astimezone(pytz.utc)
		except(ValueError):
			try:			
				time =  datetime.datetime.strptime(dt, "%d/%m/%Y %I:%M %p %z").astimezone(pytz.utc)
			except(ValueError):			
				time = 	datetime.datetime.strptime(dt, "%d/%m/%Y %H:%M:%S").astimezone(pytz.utc)
		student_in_comm = db.session.query(Users, UserCommunity, Students).filter(Users.id == UserCommunity.uid).\
								filter(UserCommunity.uid == Students.user).filter(UserCommunity.cid == community).all()
		if student_in_comm is None or not student_in_comm:
			return False
		notif = Notifications(topic, schedule, time, faculty_id)
		db.session.add(notif)
		db.session.commit()
		notif_id = db.session.query(Notifications).filter(Notifications.schedule == schedule).first().id
		for student in student_in_comm:
			un_map = UserNotifMaps(notif_id, student[0].id)
			db.session.add(un_map)
			db.session.commit()
		return True
	except Exception:
		return False
	return True

@notifs.route('/api/notifications/get', methods = ['POST'])
@jwt_required
def get_notif():
	user = get_jwt_identity()
	try:
		now = datetime.datetime.utcnow()
		user_notif_list = db.session.query(Notifications, UserNotifMaps).filter(UserNotifMaps.user == user).filter(UserNotifMaps.valid == True)\
									.filter(Notifications.id == UserNotifMaps.notification).filter(Notifications.datetime > now).all()
		resp = []
		notif_list = []
		if user_notif_list is not None:
			for notif in user_notif_list:
				notif_list.append(notif[0].id)
				faculty_name = db.session.query(Faculty, Notifications, Users).filter(Notifications.id == notif[0].id).filter(Faculty.id == Notifications.faculty)\
												.filter(Faculty.user == Users.id).first()[2]
				schedule_title = db.session.query(Schedules).filter(Schedules.id == notif[0].schedule).first()
				resp.append({'id': notif[0].id, 'topic': notif[0].topic, 'faculty_fname': faculty_name.fname, 'faculty_lname': faculty_name.lname, 'datetime': notif[0].datetime,
							'schedule_id': notif[0].schedule, 'schedule_title': schedule_title.title })
		if resp is None or not resp:
			return jsonify([{'success': 1, 'count':  0}]), 200

		#ok, now we mark notifs as displayed!
		notif_list = set(notif_list)
		for notif in notif_list:
			db.session.query(UserNotifMaps).filter(UserNotifMaps.notification == notif).filter(UserNotifMaps.user == user).update({"valid": False})
		db.session.commit()
		return jsonify([{'success': 1, 'count': len(resp)}, resp])
	except KeyError:
		return jsonify([{ 'success':  -2 }]), 201 #DB error
	return jsonify([{ 'success':  -3 }]) #fallback