#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  schedule.py
#  
#  Copyright 2017 salaciouscrumb <salaciouscrumb@salaciouscrumb-Inspiron-3537>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
from __init__ import app
from flask import Blueprint, request, flash, url_for, redirect, render_template, jsonify, session
from sqlalchemy import exc, or_, and_, desc
from user_identity import UserIdentity
import pytz
import json, random, jsonpickle
from notifications import add_notif
import re, datetime, uuid
from models import Users, Faculty, Schedules, DiscThreads,UserCommunity, Communities
from models import db
from flask_jwt_extended import (
    JWTManager, jwt_required, create_access_token,
    jwt_refresh_token_required, create_refresh_token,
    get_jwt_identity, set_access_cookies, get_jwt_claims,
    set_refresh_cookies, unset_jwt_cookies
)
import traceback
import html

schedules = Blueprint('schedules', __name__)

def complete(form, reqFields):
	'''
	Check if the form submitted by the user is fully filled

	:param list form: The list submitted by the client
	:param list reqfields: The compulsory fields to be present
	:return: Assertion whether the form is completely filled or not
	:rtype:Boolean
	'''


	for field in reqFields:
		if field not in form or not form[field]:
			return False
	return True

def upload_schedule_fn(form, user, faculty_id):
	
   
	thread_id = uuid.uuid4().urn[9:29]
	title = form['title'].replace('\n'," ")
	community = int(form['community'])
	content = form['content'].replace('\n'," ")
	link = form['link']
	print(form['datetime'])
	if len(title.strip()) <= 0:
		return jsonify({'success': -1}), 201 #bad request
	private = db.session.query(Communities).filter(Communities.id == community).first().private
	valid = True
	if private:
		valid = db.session.query(UserCommunity).filter(UserCommunity.cid == community).\
												filter(UserCommunity.uid == user).first()
	if valid is None:
		return jsonify({'success':  -2}) #faculty cannot post in this community
	try: 
		time = datetime.datetime.strptime(form['datetime'], "%d/%m/%Y %H:%M:%S %z").astimezone(pytz.utc)
	except(ValueError):
		try:			
			time =  datetime.datetime.strptime(form['datetime'], "%d/%m/%Y %I:%M %p %z").astimezone(pytz.utc)
		except(ValueError):			
			time = 	datetime.datetime.strptime(form['datetime'], "%d/%m/%Y %H:%M:%S").astimezone(pytz.utc)
	now = datetime.datetime.utcnow()
	target = datetime.datetime(time.year, time.month, time.day, time.hour, time.minute, time.second)
	if target < now or target - now > datetime.timedelta(days=365):
		return jsonify({'success': -2 }), 201 #wrong date time
	schedule = Schedules(title,content, link, faculty_id, time, community, thread_id)
	thread = DiscThreads(thread_id, community)
	try:
		db.session.add(thread)
		db.session.commit()
		db.session.add(schedule)
		db.session.commit()
		cur_schedule = db.session.query(Schedules).filter(Schedules.thread == thread.id).first()
		notify = add_notif(user, form['datetime'], cur_schedule.id, faculty_id, community, form['title'])
		if not notify:
			return jsonify({'success': 2, 'thread_id': thread.id, 'schedule_id': cur_schedule.id})
		else: return jsonify({'success': 1, 'thread_id': thread.id, 'schedule_id': cur_schedule.id})
	except Exception:
		db.session.rollback()
		return jsonify({'success': -3 })

@schedules.route('/api/schedules/upload', methods = ['POST'])
@jwt_required
def upload_schedule():
	user = get_jwt_identity()
	faculty_id = UserIdentity.get_faculty_id(get_jwt_claims(), user)
	if faculty_id == -1:
		return jsonify({ 'success': -1 }), 401 #invalid access
	try:
		if complete(request.form, ['title', 'content', 'link', 'datetime', 'community']) == False:
			return jsonify({'success': 0 }), 201
		return upload_schedule_fn(request.form, user,faculty_id)
	except Exception as e:
		return jsonify({ 'success':  -4 }), 201
	return jsonify({ 'success':  -5 })

@schedules.route('/api/schedules/get', methods = ['POST'])
@jwt_required
def get_schedules_community():
	user = get_jwt_identity()
	try:
		if complete(request.form, ['community']) == False:
			return jsonify([{'success': 0 }]), 201
		community_id = int(request.form['community'])
		if community_id < 0:
			return jsonify([{'success':  -1}]) #no results
		check_valid = [True]
		if type == 0:
			check_valid = db.session.query(Communities, UserCommunity, Users).filter(Communities.id == UserCommunity.cid).filter(Communities.id == community_id).\
													filter(UserCommunity.uid == Users.id).first()
		else:	
			private = db.session.query(Communities).filter(Communities.id == community_id).first().private
			if private == 1:
				check_valid = db.session.query(Communities, UserCommunity, Users).filter(Communities.id == UserCommunity.cid).filter(Communities.id == community_id).\
													filter(UserCommunity.uid == Users.id).first()
		if check_valid is None or len(check_valid) == 0:
			return jsonify([{'success':  -1}]) #no results
		results = db.session.query(Communities, Schedules, Faculty, Users).filter(Schedules.community == Communities.id).filter(Communities.id == community_id).\
																		filter(Schedules.faculty == Faculty.id).filter(Faculty.user == Users.id).all()
		resp = []
		for result in results:
			resp.append({'id': result[1].id, 'title': html.unescape(result[1].title), 'content': html.unescape(result[1].content), 'link': result[1].link, 
							'faculty_fname': result[3].fname, 'faculty_lname': result[3].lname, 'thread': result[1].thread, 'timestamp': result[1].timestamp })
		return jsonify([{'success':  1}, resp]), 200
	except KeyError:
		return jsonify([{ 'success':  -2 }]), 201 #DB error
	return jsonify([{ 'success':  -3 }]) #fallback