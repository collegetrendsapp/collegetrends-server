#!bin/python3
# -*- coding: utf-8 -*-
#
#  unittests.py
# 

import unittest
from data_entry import test_data_material,test_data,test_data_students,test_data_faculty,test_data_thread,test_data_admin
import requests
from login import login_student,login_faculty
import json, time
import flask_utils
import pdb
from material import upload_material_fn
from notifications import add_notif
from __init__ import app
from flask_jwt_extended import (
	JWTManager, jwt_required, create_access_token,
	jwt_refresh_token_required, create_refresh_token,
	get_jwt_identity, set_access_cookies, get_jwt_claims,
	set_refresh_cookies, unset_jwt_cookies
)
from db_config import db
from models import Users, Faculty, Admins, Students,DiscThreads,Notifications
from flask import jsonify
import subprocess
import os
import datetime
import pytz


class College_Trends_Unit_Test(unittest.TestCase):
	
	test_data()
	test_data_students()
	test_data_faculty()
	test_data_thread()
	test_data_admin()
	def test_login_students(self):
		payload = {"mobile": "9000010002","pwd": "123456","remember": "true"}
		try:
			result=requests.post("http://localhost:7000/token/login/student",data=payload)
		except requests.exceptions.ConnectionError:
			 print("Connection Refused")
		data=json.loads(result.text)
		self.assertEqual(data['success'],1)
	def test_login_faculty(self):
		payload = {"mobile": "9000020002","pwd": "123456","remember": "true"}
		result=requests.post("http://localhost:7000/token/login/faculty",data=payload)
		data=json.loads(result.text)
		self.assertEqual(data['success'],1)
	def test_upload_material(self):
		with app.app_context():
			
			user=db.session.query(Users).filter(Users.mobile ==9000020002).first()
			session=requests.Session() 
			token=self.login_for_use(session)
			payload={'title': 'Test', 'content': 'This is a testing', 'link': 'https://docs.google.com/document/d/1srHawCPsATrc9pQKci2rzry8p1u31qwE9N1lKw_KVT0/edit?usp=sharing','community': 1, 'tag_0': "test", 'tag_1': "trial"}
			result2=session.post("http://localhost:7000/api/materials/upload",data=payload,headers={'X-CSRF-TOKEN': token})
			data=json.loads(result2.text)
			self.assertEqual(data['success'],1)

	def test_register_student(self):
		payload={"mobile": 9810535679,"pwd": "123456","stream": 1,"year": 4,"fname": "abc","lname": "xgg"}
		result=requests.post("http://localhost:7000/api/register/students",data=payload)
		data=json.loads(result.text)
		self.assertEqual(data['response'],0)

	def test_register_faculty(self):
		payload={"mobile": 9810545688,"pwd": "Calcutta","stream": 2,"fname": "Kaliprassanna","lname": "Singha"}
		result=requests.post("http://localhost:7000/api/register/faculty",data=payload)
		data=json.loads(result.text)
		self.assertEqual(data['response'],0)
	@staticmethod
	def login_for_use(session):
		payload2={"mobile": "9000020002","pwd":"123456","remember": "true"}
		result=session.post("http://localhost:7000/token/login/faculty",data=payload2)
		token=str(result.cookies).split(",")[1].split("=")[1].split()[0]
		return token
	@staticmethod
	def login_admin_use(session):
		payload2={"mobile": "8000030001","pwd":"123456","remember": "true"}
		result=session.post("http://localhost:7000/token/login/admin",data=payload2)
		token=str(result.cookies).split(",")[1].split("=")[1].split()[0]
		return token

	def test_view_thread(self):
		with app.app_context():
			session=requests.Session()
			token=self.login_for_use(session)
			thread=db.session.query(DiscThreads).first().id
			payload={"thread": thread}
			result=session.post("http://localhost:7000/api/discuss/view",data=payload,headers={'X-CSRF-TOKEN': token})
			data=json.loads(result.text)
			self.assertEqual(data[0]['success'],1)
			payload={"thread": thread,"content": "This is Test"}
			result=session.post("http://localhost:7000/api/discuss/comment",data=payload,headers={'X-CSRF-TOKEN': token})
			data=json.loads(result.text)
			self.assertEqual(data['success'],1)
	def test_upload_schedule(self):
		with app.app_context():
			session=requests.Session()
			token=self.login_for_use(session)
			time="12/05/2018 12:00:00 +0530"
			payload={"title": " Hello Testing","content": "The Test hath thus begun","link": "https://drive.google.com/open?id=0B3ZjNtdTJnC6ZXJGWnYyczZVRDA","datetime": time,"community": 1}
			result=session.post("http://localhost:7000/api/schedules/upload",data=payload,headers={'X-CSRF-TOKEN': token})
			data=json.loads(result.text)
			self.assertEqual(data['success'],1)
	def test_make_community(self):
		with app.app_context():
			session=requests.Session()
			token=self.login_admin_use(session)
			payload={"name": "Literature","private": 0, "parent": 1, "users": "58,59,60"}
			result=session.post("http://localhost:7000/api/communities/create",data=payload,headers={'X-CSRF-TOKEN': token})
			data=json.loads(result.text)
			self.assertEqual(data['success'],1)

if __name__=='__main__' :
   unittest.main()
  