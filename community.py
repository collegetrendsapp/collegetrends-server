#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  community.py
#  
#  Copyright 2017 salaciouscrumb <salaciouscrumb@salaciouscrumb-Inspiron-3537>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
from __init__ import app
from flask import Blueprint, request, flash, url_for, redirect, render_template, jsonify, session
from sqlalchemy import exc, or_, and_, desc
import json, random, jsonpickle
import re, datetime
from models import Users, Communities, UserCommunity, Faculty
from user_identity import UserIdentity
from models import db
from user_identity import UserIdentity
from flask_jwt_extended import (
    JWTManager, jwt_required, create_access_token,
    jwt_refresh_token_required, create_refresh_token,
    get_jwt_identity, set_access_cookies, get_jwt_claims,
    set_refresh_cookies, unset_jwt_cookies
)
import traceback


communities = Blueprint('communities', __name__)

def complete(form, reqFields):
	for field in reqFields:
		if field not in form or not form[field]:
			return False
	return True

@communities.route('/api/communities/list_user', methods = ['POST'])
@jwt_required
def list_user_community():
	user = get_jwt_identity()
	admin_id = UserIdentity.get_admin_id(get_jwt_claims(), user)
	if admin_id == -1:
		return jsonify({ 'success': -1 }), 401 #invalid access
	if not complete(request.form, ['community']):
		return jsonify({'success': 0 }), 201
	community = int(request.form['community'])
	private = db.session.query(Communities).filter(Communities.id == community).first().private
	user_list = []
	user_name_list = []
	user_type_list = []
	resp = db.session.query(UserCommunity).filter(UserCommunity.cid == community).all()

	if resp:
		for usern in resp: 
			user_list.append(usern.uid)
	if 'strict' in request.form:
		strict = int(request.form['strict'])
	else:
		strict = 0
	faculty = db.session.query(Faculty).all()	
	if not strict and not private and faculty:
		for fac in faculty:
			user_list.append(fac.user)
	user_list = set(user_list)
	for uid in user_list:
		u_obj = db.session.query(Users).filter(Users.id == uid).first()
		user_name_list.append(u_obj.fname + " " + u_obj.lname)
		user_type_list.append(u_obj.role)
	if len(user_list) <= 0:
		return jsonify({'success': -2}), 200
	return jsonify({'success': 1, 'users': str(user_list), 'usernames': str(user_name_list), 'types': str(user_type_list)}), 200

@communities.route('/api/communities/create', methods = ['POST'])
@jwt_required
def make_community():
	user = get_jwt_identity()
	admin_id = UserIdentity.get_admin_id(get_jwt_claims(), user)
	if admin_id == -1:
		return jsonify({ 'success': -1 }), 401 #invalid access
	if not complete(request.form, ['name', 'private', 'users']):
		return jsonify({'success': 0 }), 201
	#first community
	private = int(request.form['private'])
	name = request.form['name']
	if private:
		private = True
	else: private = False
	try:
		user_list = request.form['users'].split(",")
		for u in user_list:
			u.strip()
		user_list.append(user)
		user_list = set(user_list)
		if len(user_list) < 2:
			return jsonify({'success': -2 })
		if 'parent' not in request.form or request.form['parent'] == -1:
			community = Communities(name, private)
		else:
			community = Communities(name, request.form['parent'], private)
		db.session.add(community)
		db.session.commit()
		cur_comm = db.session.query(Communities).filter(Communities.name == name).first().id
		for uc in user_list:
			u_comm = UserCommunity(int(uc), cur_comm)
			db.session.add(u_comm)
			db.session.commit()
		return jsonify({'success': 1, 'community_id': cur_comm}), 200
	except Exception as e:
		print(e)
		return jsonify({ 'success':  -3 }), 201

@communities.route('/api/communities/add_users', methods = ['POST'])
@jwt_required
def add_user_to_community():
	user = get_jwt_identity()
	admin_id = UserIdentity.get_admin_id(get_jwt_claims(), user)
	if admin_id == -1:
		return jsonify({ 'success': -1 }), 401 #invalid access
	if not complete(request.form, ['community', 'users']):
		return jsonify({'success': 0 }), 201
	#first community
	try:
		
		user_list = request.form['users'].split(",")
		for user in user_list:
			user.strip()
		user_list = set(user_list)
		community_id = int(request.form['community'])
		cur_comm = db.session.query(Communities).filter(Communities.id == community_id).first().id
		for user in user_list:
			u_comm = UserCommunity(int(user), cur_comm)
			db.session.add(u_comm)
		db.session.commit()
		return jsonify({'success': 1, 'community_id': cur_comm}), 200
	except Exception as e:
		print(e)
		return jsonify({ 'success':  -3 }), 201

@communities.route('/api/communities/remove_users', methods = ['POST'])
@jwt_required
def remove_user_from_community():
	user = get_jwt_identity()
	admin_id = UserIdentity.get_admin_id(get_jwt_claims(), user)
	if admin_id == -1:
		return jsonify({ 'success': -1 }), 401 #invalid access
	if not complete(request.form, ['community', 'users']):
		return jsonify({'success': 0 }), 201
	#first community
	try:
		
		user_list = request.form['users'].split(",")
		for user in user_list:
			user.strip()
		user_list = set(user_list)
		community_id = int(request.form['community'])
		cur_comm = db.session.query(Communities).filter(Communities.id == community_id).first().id
		count = 0
		fail = 0
		for user in user_list:
			try:
				db.session.query(UserCommunity).filter(UserCommunity.uid == user).filter(UserCommunity.cid == community_id).delete()
				db.session.commit()
				count = count + 1
			except Exception:
				fail = fail + 1
		
		return jsonify({'success': 1, 'deleted': count, 'failed': fail}), 200
	except Exception:
		return jsonify({ 'success':  -3 }), 201

@communities.route('/api/communities/list', methods = ['POST'])
@jwt_required
def get_community():
	user = get_jwt_identity()
	type = 0
	if get_jwt_claims()['roles'] == Users.ROLE_STUDENT:
		type = 1
	elif get_jwt_claims()['roles'] == Users.ROLE_FACULTY:
		type = 2
	cid = 0
	try:
		cid = int(request.form['cid'])
	except Exception:
		cid = -1
	try:
		if cid > 0:  #subcommunity
			results = db.session.query(Communities, UserCommunity, Users).filter(Communities.id == UserCommunity.cid).filter(Communities.parent == cid).\
														filter(UserCommunity.uid == Users.id).filter(Users.id == user).all()
		else:
			results = db.session.query(Communities, UserCommunity, Users).filter(Communities.id == UserCommunity.cid).filter(Communities.parent.is_(None)).\
														filter(UserCommunity.uid == Users.id).filter(Users.id == user).all()
		if type != 1:
			if cid > 0:  #subcommunity
				results_aux = db.session.query(Communities).filter(Communities.parent == cid).\
														filter(Communities.private == False).all()
			else:
				results_aux = db.session.query(Communities).filter(Communities.private == False).filter(Communities.parent.is_(None)).all()	
		if (type == 1 and results is None) or (type != 1 and results is None and results_aux is None):
			return jsonify([{'success':  -1}])
		id_list = []
		for result in results: id_list.append(result[0].id)
		if type != 1:
			for result in results_aux: id_list.append(result.id)
		id_list = set(id_list)
		resp = []
		for result in id_list:
			info = db.session.query(Communities).filter(Communities.id == result).first()
			resp.append({'id': info.id, 'name': info.name})
		return jsonify([{'success':  1}, resp]), 200
	except Exception as e:
		return jsonify([{ 'success':  -2 }]), 201
	return jsonify([{ 'success':  -3 }])

def community_access(user_id,comm_id) :
	pass
@communities.route('/api/communities/list_user_exc', methods = ['POST'])
@jwt_required
def list_user_community_exc():
	user = get_jwt_identity()
	admin_id = UserIdentity.get_admin_id(get_jwt_claims(), user)
	if admin_id == -1:
		return jsonify({ 'success': -1 }), 401 #invalid access
	if not complete(request.form, ['community', 'parent']):
		return jsonify({'success': 0 }), 201
	community = int(request.form['community'])
	parent = int(request.form['parent'])
	private = db.session.query(Communities).filter(Communities.id == community).first().private
	if parent != -1: private_p = db.session.query(Communities).filter(Communities.id == parent).first().private
	else: private_p = False
	user_list = []
	user_name_list = []
	user_type_list = []
	if parent != -1:
		resp = db.session.query(UserCommunity).filter(UserCommunity.cid == parent).all()
		if resp:
			for usern in resp: 
				user_list.append(usern.uid)
	else:
		resp = db.session.query(Users).all()
		if resp:
			for usern in resp: 
				user_list.append(usern.id)
	faculty = db.session.query(Faculty).all()	
	if private and not private_p and faculty:
		for fac in faculty:
			user_list.append(fac.user)
	user_list = set(user_list)

	inc_list = []
	already_inc =  db.session.query(UserCommunity).filter(UserCommunity.cid == community).all()
	if already_inc:
		for us in already_inc:
			inc_list.append(us.uid)
	inc_list = set(inc_list)
	user_list = user_list.difference(inc_list)
	for uid in user_list:
		u_obj = db.session.query(Users).filter(Users.id == uid).first()
		user_name_list.append(u_obj.fname + " " + u_obj.lname)
		user_type_list.append(u_obj.role)
	if len(user_list) <= 0:
		return jsonify({'success': -2}), 200
	return jsonify({'success': 1, 'users': str(user_list), 'usernames': str(user_name_list), 'types': str(user_type_list)}), 200