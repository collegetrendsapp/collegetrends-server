import community
import unittest

class Community(unittest.TestCase):

	def test_community_access(user_id) :
		result=community.community_access(user_id)
		self.assertEqual(True,result)
    
if __name__=='main' :
	unittest.main()
