#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  data_entry.py
#  
#  Copyright 2017 salaciouscrumb <salaciouscrumb@salaciouscrumb-Inspiron-3537>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
from __init__ import app
from flask import Blueprint, request, flash, url_for, redirect, render_template, jsonify, session
from sqlalchemy import exc
import re
from models import Users, Students, Streams, Communities, StreamYears,DiscThreads,ThreadPosts
from models import db
from flask_jwt_extended import (
    JWTManager, jwt_required, create_access_token,
    jwt_refresh_token_required, create_refresh_token,
    get_jwt_identity, set_access_cookies,
    set_refresh_cookies, unset_jwt_cookies
)
from registration import register_student_fn, register_faculty_fn, register_admin_fn
from material import upload_material_fn
import traceback
dataentry = Blueprint('dataentry', __name__)

class DataEntry:

    @staticmethod
    def stream_input(sub_long, sub_short):
        #strategy --> create communities, then create streams
        try:
            sub_com = Communities(sub_long)
            db.session.add(sub_com)
            db.session.commit()
            community_id = db.session.query(Communities).filter(Communities.name == sub_long).filter(Communities.parent.is_(None)).first().id
            sub_stream = Streams(sub_short, community_id)
            db.session.add(sub_stream)
            db.session.commit()
            return 1
        except Exception:
            db.session.rollback()
            return 0

    @staticmethod
    def year_input(stream_long, stream_short):
        #again, first we need the community, then the year
        try:
            stream_id = db.session.query(Streams).filter(Streams.name == stream_short).first().id
            stream_com = db.session.query(Communities).filter(Communities.name == stream_long).filter(Communities.parent.is_(None)).first().id
            string_year = ['1st', '2nd', '3rd', '4th', '5th', '6th', '7th', '8th', '9th', '10th']
            for i in range(1,5):
                comm_name = stream_short + " " + string_year[i - 1] + " year"
                year_com = Communities(comm_name, stream_com)
                db.session.add(year_com)
                db.session.commit()
                community_id = db.session.query(Communities).filter(Communities.name == comm_name).filter(Communities.parent == stream_com).first().id

                year = StreamYears(i, stream_id, community_id)
                db.session.add(year)
                db.session.commit()
            return 1
        except Exception:
            db.session.rollback()
            return 0

@dataentry.route('/api/test/data/streams', methods = ['GET'])
def test_data():
    stream_long = ['Computer Science and Engineering', 'Electronics and Communications Engineering', 'Information Technology', 'Mechanical Engineering', 'Electrical Engineering']
    stream_short = ['CSE', 'ECE', 'IT', 'ME', 'EE']
    success = 0
    for i in range(0,5):
        res = DataEntry.stream_input(stream_long[i], stream_short[i])
        if res:
            ans = DataEntry.year_input(stream_long[i], stream_short[i])
            if res and ans:
                success = success + 1
    return "<html><head><title>College Trends</title></head><body>Successes: " + str(success) + "<body></html>"

@dataentry.route('/api/test/data/students', methods = ['GET'])
def test_data_students():
    success = 0
    for i in range(1, 21):
        try:
            mob = str(9000010000 + i)
            register_student_fn({'mobile': mob, 'pwd': '123456', 'fname': 'User', 'lname': str(i), 'stream': 1, 'year': 4})
            success = success + 1
        except Exception:
            pass

    for i in range(1, 21):
        try:
            mob = str(8000010000 + i)
            register_student_fn({'mobile': mob, 'pwd': '123456', 'fname': 'User', 'lname': str(20 + i), 'stream': 2, 'year': 4})
            success = success + 1
        except Exception:
            pass
    return "<html><head><title>College Trends</title></head><body>Successes: " + str(success) + "<body></html>"

@dataentry.route('/api/test/data/faculty', methods = ['GET'])
def test_data_faculty():
    success = 0
    for i in range(1, 11):
        try:
            mob = str(9000020000 + i)
            register_faculty_fn({'mobile': mob, 'pwd': '123456', 'fname': 'Faculty', 'lname': str(i), 'stream': 1})
            success = success + 1
        except Exception:
            pass

    for i in range(1, 11):
        try:
            mob = str(8000020000 + i)
            register_faculty_fn({'mobile': mob, 'pwd': '123456', 'fname': 'Faculty', 'lname': str(10 + i), 'stream': 2})
            success = success + 1
        except Exception:
            pass

    return "<html><head><title>College Trends</title></head><body>Successes: " + str(success) + "<body></html>"

@dataentry.route('/api/test/data/material', methods = ['GET'])
def test_data_material():
    return upload_material_fn({'title': 'Test', 'content': 'This is a testing', 'link': 'https://docs.google.com/document/d/1srHawCPsATrc9pQKci2rzry8p1u31qwE9N1lKw_KVT0/edit?usp=sharing',
                        'community': 1, 'tag_0': "test", 'tag_1': "trial"}, 52, 1)

@dataentry.route('/api/test/data/admin', methods = ['GET'])
def test_data_admin():
    success = 0
    for i in range(1, 2):
        try:
            mob = str(900003000 + i)
            register_admin_fn({'mobile': mob, 'pwd': '123456', 'fname': 'Admin', 'lname': '1'})
            success = success + 1
        except Exception:
            pass


    for i in range(1, 2):
        try:
            mob = str(8000030000 + i)
            register_admin_fn({'mobile': mob, 'pwd': '123456', 'fname': 'Admin', 'lname': '2'})
            success = success + 1
        except Exception:
            pass

    return "<html><head><title>College Trends</title></head><body>Successes: " + str(success) + "<body></html>"
@dataentry.route('/api/test/data/thread', methods = ['GET'])
def test_data_thread():
    try:
        thread=db.session.query(DiscThreads).first().id
        timestamp=db.session.query(DiscThreads).first().timestamp
        user=db.session.query(Users).filter(Users.mobile==9000020002).first().id
        role=db.session.query(Users).filter(Users.mobile==9000020002).first().role
        content="I have thus said something"
        post=ThreadPosts(thread,user,role,content)
        db.session.add(post)
        db.session.commit()
    except Exception as e:
        db.session.rollback()

