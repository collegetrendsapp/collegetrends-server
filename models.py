#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  models.py
#  
#  Copyright 2017 salaciouscrumb <salaciouscrumb@salaciouscrumb-Inspiron-3537>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#
import hashlib, uuid, re
from datetime import datetime
from db_config import db
from sqlalchemy import UniqueConstraint, PrimaryKeyConstraint, exc
import cgi, html

class Users(db.Model):
	ROLE_STUDENT = 0
	ROLE_FACULTY = 1
	ROLE_ADMIN = 2
	id = db.Column('user_id', db.Integer, primary_key = True)
	fname = db.Column('user_fname', db.Text)
	lname = db.Column('user_lname', db.Text)
	mobile = db.Column('user_mobile', db.BIGINT, unique = True)
	email = db.Column('user_email', db.Text, unique = True)
	pwd = db.Column('user_pwd', db.Text) 
	salt = db.Column('user_salt', db.Text)
	role = db.Column('user_role', db.Text)
	
	def is_authenticated(self):
		return True
 
	def is_active(self):
		return True
	
	def is_anonymous(self):
		return False
	
	def get_id(self):
		return str(self.id)
		
	def __init__(self, mobile, fname, lname, email, pwd, role):
		self.mobile =  mobile
		self.fname = html.escape(fname)
		self.lname = html.escape(lname)
		if len(email) == 0:
			self.email = None
		else: self.email = html.escape(email)
		self.salt = uuid.uuid4().hex
		self.pwd = hashlib.sha512((pwd + self.salt).encode('utf-8')).hexdigest()
		self.role = int(role)

class Students(db.Model):
	id = db.Column('student_id', db.Integer, primary_key = True)
	user = db.Column('student_user_id', db.Integer, unique = True)
	stream = db.Column('student_stream', db.Integer)
	year = db.Column('student_year', db.Integer)

	def __init__(self, curUser, stream, year):
		self.user = curUser.id
		self.stream = stream
		self.year = year

class Admins(db.Model):
	id = db.Column('admin_id', db.Integer, primary_key = True)
	user = db.Column('admin_user_id', db.Integer, unique = True)

	def __init__(self, curUser):
		self.user = curUser.id

class Faculty(db.Model):
	id = db.Column('faculty_id', db.Integer, primary_key = True)
	user = db.Column('faculty_user_id', db.Integer, unique = True)
	stream = db.Column('faculty_stream', db.Integer)

	def __init__(self, curUser, stream):
		self.user = curUser.id
		self.stream = stream

class UserToken(db.Model):
	__tablename__ = 'user_token'
	__table_args__ = ( PrimaryKeyConstraint('token_user_id', 'token_refresh'), )
	user_id = db.Column('token_user_id', db.Integer)
	refresh = db.Column('token_refresh', db.String(200))
	expiry = db.Column('token_expiry', db.DateTime)
	agent = db.Column('token_agent', db.String(150))
	timestamp = db.Column('token_timestamp', db.DateTime)
	
	def __init__(self, uid, refresh, agent, expiry):
		self.user_id = uid
		self.refresh = hashlib.sha512((refresh).encode('utf-8')).hexdigest()
		self.agent = agent
		self.expiry = expiry
		
class Streams(db.Model):
	id = db.Column('stream_id', db.Integer, primary_key = True)
	name = db.Column('stream_name', db.String(40))
	community = db.Column('stream_community', db.Integer)

	def __init__(self, name, community):
		self.name = html.escape(name)
		self.community = int(community)

class StreamYears(db.Model):
	__tablename__ = 'stream_years'
	__table_args__ = ( PrimaryKeyConstraint('year_no', 'year_stream_id'), )
	id = db.Column('year_id', db.Integer)
	no = db.Column('year_no', db.Integer)
	stream = db.Column('year_stream_id', db.Integer)
	community = db.Column('year_community', db.Integer)

	def __init__(self, no, stream, community):
		if no < 0:
			no = 1 #safety check if other tests fail
		if no > 10:
			no = 10
		self.no = int(no)
		self.stream = int(stream)
		self.community = int(community)

class Communities(db.Model):
	id = db.Column('community_id', db.Integer, primary_key = True)
	name = db.Column('community_name', db.String(100))
	parent = db.Column('community_parent', db.Integer)
	private = db.Column('community_private', db.Boolean)

	def __init__(self, name, parent=None, private=False):
		self.name = html.escape(name)
		if parent is not None:
			self.parent = int(parent)
		else:
			self.parent = None
		if private is not None:
			self.private = bool(private)
		else:
			self.private = None

class UserCommunity(db.Model):
	__tablename__ = 'user_community'
	__table_args__ = ( PrimaryKeyConstraint('uc_user_id', 'uc_community_id'), )
	uid = db.Column('uc_user_id', db.Integer)
	cid = db.Column('uc_community_id', db.Integer)
	
	def __init__(self, uid, cid):
		self.uid = int(uid)
		self.cid = int(cid)

class Materials(db.Model):
	id = db.Column('material_id', db.Integer, primary_key = True)
	title = db.Column('material_title', db.String(400))
	content = db.Column('material_content', db.Text)
	link = db.Column('material_link', db.String(400))
	faculty = db.Column('material_faculty', db.Integer)
	community = db.Column('material_community', db.Integer)
	thread = db.Column('material_thread', db.String(20))
	timestamp = db.Column('material_timestamp', db.DateTime)

	def __init__(self, title, content, link, faculty, community, thread):
		self.title = html.escape(title)
		self.content = html.escape(content)
		self.link = html.escape(link)
		self.faculty = int(faculty)
		self.community = int(community)
		self.thread = thread

class Schedules(db.Model):
	id = db.Column('schedule_id', db.Integer, primary_key = True)
	title = db.Column('schedule_title', db.String(400))
	type = db.Column('schedule_type', db.Integer)
	content = db.Column('schedule_content', db.Text)
	link = db.Column('schedule_link', db.String(400))
	datetime = db.Column('schedule_datetime', db.DateTime)
	faculty = db.Column('schedule_faculty', db.Integer)
	community = db.Column('schedule_community', db.Integer)
	thread = db.Column('schedule_thread', db.String(20))
	timestamp = db.Column('schedule_timestamp', db.DateTime)

	def __init__(self, title, content, link, faculty, datetime, community, thread):
		self.title = html.escape(title)
		self.content = html.escape(content)
		self.link = html.escape(link)
		self.faculty = int(faculty)
		self.community = int(community)
		self.datetime = datetime
		self.type = 1 #to be implemented
		self.thread = thread

class DiscThreads(db.Model):
	__tablename__ = 'disc_threads'
	id = db.Column('disc_thread_id', db.String(20), primary_key = True)
	community = db.Column('disc_thread_community', db.Integer)
	timestamp = db.Column('disc_thread_timestamp', db.DateTime)

	def __init__(self, id, community):
		self.community = int(community)
		self.id = id

class Tags(db.Model):
	id = db.Column('tag_id', db.Integer, primary_key = True)
	name = db.Column('tag_name', db.String(40))

	def __init__(self, name):
		self.name = html.escape(name).lower()

class MaterialTags(db.Model):
	__tablename__ = 'material_tags'
	__table_args__ = ( PrimaryKeyConstraint('mt_material_id', 'mt_tag_id'), )
	material = db.Column('mt_material_id', db.Integer)
	tag = db.Column('mt_tag_id', db.Integer)

	def __init__(self, material, tag):
		self.material = int(material)
		self.tag = int(tag)

class UserNotifMaps(db.Model):
	__tablename__ = 'un_notif_maps'
	__table_args__ = ( PrimaryKeyConstraint('un_notif_id', 'un_user_id'), )
	notification = db.Column('un_notif_id', db.Integer)
	user = db.Column('un_user_id', db.Integer)
	valid = db.Column('un_valid', db.Boolean)

	def __init__(self, notif, user):
		self.notification = int(notif)
		self.user = int(user)
		self.valid = True

class Notifications(db.Model):
	id = db.Column('notif_id', db.Integer, primary_key = True)
	topic = db.Column('notif_topic',db.String(400))
	schedule = db.Column('notif_schedule', db.Integer)
	datetime = db.Column('notif_datetime', db.DateTime)
	faculty = db.Column('notif_faculty', db.Integer)
	timestamp = db.Column('notif_timestamp', db.DateTime)	

	def __init__(self, topic, schedule, datetime, faculty):
		self.schedule = int(schedule)
		self.datetime = datetime
		self.faculty = int(faculty)
		self.topic = html.escape(topic)

class ThreadPosts(db.Model):
	__tablename__ = 'thread_posts'
	id = db.Column('post_id', db.Integer, primary_key = True)
	thread = db.Column('post_thread_id',db.String(20))
	parent = db.Column('post_parent_id', db.Integer)
	user = db.Column('post_user', db.Integer)
	userlevel  = db.Column('post_user_level', db.Integer)
	content = db.Column('post_content', db.Text)
	timestamp = db.Column('post_timestamp', db.DateTime)

	def __init__(self, thread, user, userlevel, content):
		self.thread = thread #already valid
		self.parent = None
		self.user = int(user)
		self.userlevel = int(userlevel)
		self.content = html.escape(content)